﻿using ReflectionUnit.Core.Implemenatation;
using ReflectionUnit.Infrastrusture.Abstraction;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ReflectionUnit.Infrastrusture.Implementation;

internal class Controller : IController
{
    public void run()
    {
        var testObject= new F().Get();

        CSV(testObject);

        Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++");

        testObject= new F().Get();

        JSON(testObject);

        Console.WriteLine("Press any key to exit");

        Console.ReadKey();

    }

    private void CSV( F testObject)
    {
        var stopwatch = new Stopwatch();
            
        stopwatch.Start();        

        var csvf = new CSVFormatter();

        Console.WriteLine($"CSV serialization-deserializtion");

        Console.WriteLine($"before: {testObject}");
        
        for (int i = 0; i < 100000; i++)
        {
            using (var fs = new FileStream("csvSerializaton.csv", FileMode.Create))
            {
                csvf.Serialize(fs, testObject);
            }
        }
        
        stopwatch.Stop();

        Console.WriteLine($"CSV 100000 times ser: {stopwatch.ElapsedMilliseconds}ms");

        stopwatch.Reset();
        

        stopwatch.Start();

        F after=new F().Get();

        for (int i = 0; i < 100000; i++)
        {
            using (var fs = new FileStream("csvSerializaton.csv", FileMode.Open))
            {
              after = (F)csvf.Deserialize(fs);

            }
        }
        
        Console.WriteLine($"after: {after}");

        stopwatch.Stop();

        Console.WriteLine($"CSV 100000 times deser: {stopwatch.ElapsedMilliseconds}ms");

    }
        
     

    private void JSON(F testObject)
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        Console.WriteLine($"JSON serialization-deserializtion");

         Console.WriteLine($"before: {testObject}");
        
        for (int i = 0; i < 100000; i++)
        {
           var json = JsonSerializer.Serialize(testObject);
           File.WriteAllText("json.json", json);
        }


        stopwatch.Stop();

        Console.WriteLine($"json 100000 times ser: {stopwatch.ElapsedMilliseconds} ms");

        stopwatch.Reset();
        stopwatch.Start();
        
        F after=new F().Get();

        for (int i = 0; i < 100000; i++)
        {
           after = JsonSerializer.Deserialize<F>(File.ReadAllText("json.json"));
        }

        Console.WriteLine($"after: {after}");

        stopwatch.Stop();
        Console.WriteLine($"json  100000 times  deser: {stopwatch.ElapsedMilliseconds}ms");

    }
}
