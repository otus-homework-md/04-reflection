﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionUnit.Infrastrusture.Abstraction;

internal interface IController
{
    void run();

}
