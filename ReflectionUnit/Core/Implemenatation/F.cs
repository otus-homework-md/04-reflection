﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.Runtime.Serialization;

namespace ReflectionUnit.Core.Implemenatation;

[Serializable]
public class F {
   
    public int i1{get;set;}

    public int i2{get;set;}

    public int i3{get;set;}

    public int i4{get;set;}

    [JsonIgnore]
    [NonSerialized()]
    string d="Hello!";
        
    public int i5{get;set;}

    public F(){ }

    public F Get() => new F(){ i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };

    public override string ToString()
    {
        return $"i1={i1};i2={i2};i3={i3};i4={i4};i5={i5};";
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
          
    }

    public F(SerializationInfo info, StreamingContext context)
    {
           
    }
}
